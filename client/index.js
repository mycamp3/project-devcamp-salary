document.getElementById('salary-form').addEventListener('submit', async function(e) {
    e.preventDefault();

    const grossSalary = document.getElementById('grossSalary').value;
    const dependents = document.getElementById('dependents').value;
    const insuranceSalary = document.getElementById('insuranceSalary').value;
    const region = document.getElementById('region').value;

    try {
        const response = await axios.post('http://localhost:8000/gross-to-net', {
            grossSalary: parseFloat(grossSalary),
            dependents: parseInt(dependents),
            insuranceSalary: parseFloat(insuranceSalary),
            region: parseInt(region)
        });

        document.getElementById('result').innerText = `Net Salary: ${response.data.netSalary.toLocaleString('vi-VN')} VND`;
    } catch (error) {
        console.error(error);
        document.getElementById('result').innerText = 'Error calculating net salary';
    }
});
