const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 8000;

app.use(bodyParser.json());
app.use(cors());

const regions = {
    1: 4470000,
    2: 3950000,
    3: 3470000,
    4: 3070000
};

app.post('/gross-to-net', (req, res) => {
    const { grossSalary, dependents, insuranceSalary, region } = req.body;

    if (!grossSalary || !insuranceSalary || !region) {
        return res.status(400).send('Missing required fields');
    }

    const regionRate = regions[region] || 0;
    const insuranceContribution = insuranceSalary * 0.105;
    const personalDeduction = 11000000;
    const dependentDeduction = 4400000 * (dependents || 0);

    const taxableIncome = grossSalary - insuranceContribution - personalDeduction - dependentDeduction;

    const taxRates = [
        { threshold: 5000000, rate: 0.05 },
        { threshold: 10000000, rate: 0.10 },
        { threshold: 18000000, rate: 0.15 },
        { threshold: 32000000, rate: 0.20 },
        { threshold: 52000000, rate: 0.25 },
        { threshold: 80000000, rate: 0.30 },
        { threshold: Infinity, rate: 0.35 }
    ];

    let remainingIncome = taxableIncome;
    let tax = 0;

    for (const { threshold, rate } of taxRates) {
        if (remainingIncome <= 0) break;

        const taxableAtThisRate = Math.min(threshold, remainingIncome);
        tax += taxableAtThisRate * rate;
        remainingIncome -= taxableAtThisRate;
    }

    const netSalary = grossSalary - insuranceContribution - tax;

    res.send({ netSalary });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
